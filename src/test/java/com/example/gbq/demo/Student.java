package com.example.gbq.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Student {

    public String geT0(int i) throws InterruptedException {
        if(i==0){
            i++;
            Thread.sleep(100000);
            return "Yes"+i;
        }
        return "No!"+i;
    }

    @Test
    public void hello1() throws Exception {
        System.err.println(this.geT0(1));
    }

    @Test
    public void hello2() throws Exception {
        System.err.println(this.geT0(0));
    }

}
