//package com.example.gbq.demo;
//
//import com.example.gbq.demo.mq.SenderConfig;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import javax.annotation.Resource;
//
//@SpringBootTest
//public class RabbitDemoApplicationTests {
//
//    @Resource
//    private SenderConfig senderConfig;
//
//    //普通模式发送
//    @Test
//    public void contextLoads() {
//        senderConfig.send();
//    }
//
//    //工作模式
//    @Test
//    public void contextLoads2() {
//        for (int i = 0;i<10; i++){
//            senderConfig.sendToWork();
//        }
//    }
//
//    //发布订阅模式
//    @Test
//    public void contextLoads3() {
//        for (int i = 0;i<10; i++){
//            senderConfig.sendToFanout();
//        }
//    }
//
//    //路由模式
//    @Test
//    public void contextLoads4() {
//        for (int i = 0;i<10; i++){
//            senderConfig.sendToDirect();
//        }
//    }
//
//    //通配符模式
//    @Test
//    public void contextLoads5() {
//        senderConfig.sendToTopic("lazy.111");
//        senderConfig.sendToTopic("111.orange.111");
//        senderConfig.sendToTopic("1111.111.orange");
//        senderConfig.sendToTopic("1111.111.rabbit");
//        senderConfig.sendToTopic("1111.rabbit");
//    }
//
//}
