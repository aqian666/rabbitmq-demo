package com.example.gbq.demo.design.shape;

public class ShapeFactory {

    public void getShape(String shapeType){
        if(shapeType == null){

        }
        if(shapeType.equalsIgnoreCase("CIRCLE")){
            new Circle();
        } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
            new Rectangle();
        } else if(shapeType.equalsIgnoreCase("SQUARE")){
            new Square();
        }
    }
}
