package com.example.gbq.demo.design.shape;

public class FactoryPatternDemo {

    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ShapeFactory();

        //获取 Circle 的对象，并调用它的 draw 方法
        shapeFactory.getShape("CIRCLE");

        //调用 Circle 的 draw 方法

        //获取 Rectangle 的对象，并调用它的 draw 方法
        shapeFactory.getShape("RECTANGLE");

        //调用 Rectangle 的 draw 方法

        //获取 Square 的对象，并调用它的 draw 方法
       shapeFactory.getShape("SQUARE");

        //调用 Square 的 draw 方法
    }
}
