package com.example.gbq.demo.controller;

import com.example.gbq.demo.mq.DynamicMessageListener;
import com.example.gbq.demo.mq.RabbitConfig;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Properties;

@RestController
public class RabbitSendController {

    @Resource
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private RabbitConfig rabbitConfig;
    @Autowired
    private DynamicMessageListener message;


    @Autowired
    private SimpleMessageListenerContainer container;

    @GetMapping("/setRabbit")
    public void send(String queueName) {
        // Declare the queue
        String context = "手动创建队列 " + new Date();
        rabbitConfig.createQueue(queueName);

        //获取当前监听的队列名称
        container.addQueueNames(queueName);
        //设置消息监听处理类
        container.setMessageListener(message);

        //往指定队列发送消息
        rabbitTemplate.convertAndSend(queueName,context);
    }

}

