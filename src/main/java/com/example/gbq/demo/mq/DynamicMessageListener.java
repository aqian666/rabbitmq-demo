package com.example.gbq.demo.mq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.stereotype.Component;


import java.io.UnsupportedEncodingException;

@Slf4j
@Component
public class DynamicMessageListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        try {
            String mes = new String(message.getBody(), "utf-8");
            log.info("监听到消息:{}",mes);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


}


