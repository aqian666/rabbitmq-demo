//package com.example.gbq.demo.mq;
//
//import org.springframework.amqp.core.AmqpTemplate;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//
//@Component
//public class SenderConfig {
//
//    @Resource
//    private AmqpTemplate rabbitTemplate;
//
//    //创建消息发送者
//    public void send() {
//        String context = "普通模式发送的消息";
//        System.out.println("普通模式发送者: " + context);
//        this.rabbitTemplate.convertAndSend("hello", context);
//    }
//
//    //创建消息发送者
//    public void sendToWork() {
//        String context = "工作模式发送的消息";
//        System.out.println("工作模式发送者: " + context);
//        //有序发送（轮训）
////        this.rabbitTemplate.convertSendAndReceive("work", context);
//        //无序发送（公平）
//        this.rabbitTemplate.convertAndSend("work", context);
//    }
//
//    //创建消息发送者
//    public void sendToFanout() {
//        String context = "发布订阅模式发送的消息";
//        System.out.println("发布订阅模式发送者: " + context);
//        this.rabbitTemplate.convertAndSend("exchange.fanout", "",context);
//    }
//
//    //创建消息发送者
//    public void sendToDirect() {
//        String context = "路由模式发送的消息";
//        System.out.println("路由模式发送者: " + context);
//        //走black路由
//        this.rabbitTemplate.convertAndSend("exchange.direct", "black",context);
//        //走orange路由
//        // this.rabbitTemplate.convertAndSend("exchange.direct", "orange",context);
//        //走green路由
//        // this.rabbitTemplate.convertAndSend("exchange.direct", "green",context);
//    }
//
//    //创建消息发送者
//    public void sendToTopic(String index) {
//        String context = "通配符模式发送的消息";
//        System.out.println("通配符模式发送者: " + context);
//        this.rabbitTemplate.convertAndSend("exchange.topic", index,context+":"+index);
//    }
//}
