package com.example.gbq.demo.mq;// RabbitConfig.java
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.util.Properties;

@Configuration
public class RabbitConfig {

    @Autowired
    private AmqpAdmin amqpAdmin;

    public void createQueue(String queueName) {
        //判断队列是否存在
        if (!isExistQueue(queueName)) {
            //创建队列
            amqpAdmin.declareQueue(new Queue(queueName, true));
        }
    }
    /**方法描述: 判断队列是否存在
     * @author lin
     * @date 2023/4/3 15:11
     * @param queueName
     * @return boolean  true-存在
     */
    public boolean isExistQueue(String queueName){
        boolean flag = true;
        if(!StringUtils.isEmpty(queueName)){
            Properties queueProperties = amqpAdmin.getQueueProperties(queueName);
            if (queueProperties == null) {
                flag = false;
            }
        }else {
            throw new RuntimeException("队列名称为空");
        }
        return flag;
    }

    @Bean
    public SimpleMessageListenerContainer messageContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        return container;
    }


}
