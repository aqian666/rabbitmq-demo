//package com.example.gbq.demo.mq;
//
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.core.RabbitAdmin;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
////
//@Configuration
//public class RabbitConfig2 {
////
////    //申明第一个队列
////    @Bean
////    public Queue helloQueue() {
////        return new Queue("hello");
////    }
////
/////************************************************分割******************************************************/
////
////    //申明一个工作模式队列
////    @Bean
////    public Queue workQueue() {
////        return new Queue("work");
////    }
////
/////************************************************分割******************************************************/
////    //申明俩个发布订阅模式的交换机
////    @Bean
////    public FanoutExchange fanout() {
////        return new FanoutExchange("exchange.fanout");
////    }
////
////    //申明俩个发布订阅模式队列
////    @Bean
////    public Queue fanoutQueue1() {
////        return new Queue("fanout1");//队列一
////    }
////    @Bean
////    public Queue fanoutQueue2() {
////        return new Queue("fanout2");//队列二
////    }
////
////    //将队列一绑定到交换机
////    @Bean
////    public Binding fanoutBinding1(FanoutExchange fanout, Queue fanoutQueue1) {
////        return BindingBuilder.bind(fanoutQueue1).to(fanout);
////    }
////    //将队列二绑定到交换机
////    @Bean
////    public Binding fanoutBinding2(FanoutExchange fanout, Queue fanoutQueue2) {
////        return BindingBuilder.bind(fanoutQueue2).to(fanout);
////    }
////
/////************************************************分割******************************************************/
////    //声明路由模式交换机
////    @Bean
////    public DirectExchange direct() {
////        return new DirectExchange("exchange.direct");
////    }
////
////    //申明俩个路由模式队列
////    @Bean
////    public Queue directQueue1() {
////        return new Queue("direct1"); //队列一
////    }
////    @Bean
////    public Queue directQueue2() {
////        return new Queue("direct2"); //队列二
////    }
////
////    //将队列队列一绑定到交换机
////    @Bean
////    public Binding directBinding1a(DirectExchange direct, Queue directQueue1) {
////        return BindingBuilder.bind(directQueue1).to(direct).with("orange");
////    }
////    @Bean
////    public Binding directBinding1b(DirectExchange direct, Queue directQueue1) {
////        //
////        return BindingBuilder.bind(directQueue1).to(direct).with("black");
////    }
////
////    //将队列队列二绑定到交换机
////    @Bean
////    public Binding directBinding2a(DirectExchange direct, Queue directQueue2) {
////        return BindingBuilder.bind(directQueue2).to(direct).with("green");
////    }
////    @Bean
////    public Binding directBinding2b(DirectExchange direct, Queue directQueue2) {
////        return BindingBuilder.bind(directQueue2).to(direct).with("black");
////    }
/////************************************************分割******************************************************/
////
////    //声明通配符模式交换机
////    @Bean
////    public TopicExchange topic() {
////        return new TopicExchange("exchange.topic");
////    }
////    //声明俩个通配符模式队列
////    @Bean
////    public Queue topicQueue1() {
////        return new Queue("topic1");//队列一
////    }
////    @Bean
////    public Queue topicQueue2() {
////        return new Queue("topic2");//队列二
////    }
////
////    //将队列队列一绑定到交换机
////    @Bean
////    public Binding topicBinding1a(TopicExchange topic, Queue topicQueue1) {
////        return BindingBuilder.bind(topicQueue1).to(topic).with("*.orange.*");
////    }
////    @Bean
////    public Binding topicBinding1b(TopicExchange topic, Queue topicQueue1) {
////        return BindingBuilder.bind(topicQueue1).to(topic).with("*.*.rabbit");
////    }
////    //将队列队列二绑定到交换机
////    @Bean
////    public Binding topicBinding2a(TopicExchange topic, Queue topicQueue2) {
////        return BindingBuilder.bind(topicQueue2).to(topic).with("lazy.#");
////    }
////
////
//
//    @Bean
//    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
//        return new RabbitAdmin(connectionFactory);
//    }
//
//}
