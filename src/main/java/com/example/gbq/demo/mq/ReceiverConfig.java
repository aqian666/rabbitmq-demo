//package com.example.gbq.demo.mq;
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
//@Component
//public class ReceiverConfig {
//
//    //创建消息接收者
//    @RabbitListener(queues = "hello")
//    @RabbitHandler
//    public void process1(String hello) {
//        System.err.println("普通模式消费者: " + hello);
//    }
//
//
//    //创建消息接收者
//    @RabbitListener(queues = "work")
//    @RabbitHandler
//    public void process2(String work) throws InterruptedException {
////        Thread.sleep(20000);
//        System.err.println("工作模式 消费者1: " + work);
//    }
//
//    @RabbitListener(queues = "work")
//    @RabbitHandler
//    public void process3(String work) {
//        System.out.println("工作模式 消费者2: " + work);
//    }
//
//    @RabbitListener(queues = "fanout1")
//    @RabbitHandler
//    public void process4(String fanout) {
//        System.err.println("发布订阅模式 消费者1: " + fanout);
//    }
//
//    @RabbitListener(queues = "fanout2")
//    @RabbitHandler
//    public void process5(String fanout) {
//        System.out.println("发布订阅模式 消费者2: " + fanout);
//    }
//
//    @RabbitListener(queues = "direct1")
//    @RabbitHandler
//    public void process6(String direct) {
//        System.err.println("路由模式 消费者1: " + direct);
//    }
//
//    @RabbitListener(queues = "direct2")
//    @RabbitHandler
//    public void process7(String direct) {
//        System.out.println("路由模式 消费者2: " + direct);
//    }
//
//
//    @RabbitListener(queues = "topic1")
//    @RabbitHandler
//    public void process8(String topic) {
//        System.err.println("通配符模式 消费者1: " + topic);
//    }
//
//    @RabbitListener(queues = "topic2")
//    @RabbitHandler
//    public void process9(String topic) {
//        System.out.println("通配符模式 消费者2: " + topic);
//    }
//}
